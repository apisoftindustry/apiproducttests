package starter.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;

public class BaseTest {

    public static final String BASE_URL = SerenityRest.setDefaultBasePath("https://waarkoop-server.herokuapp.com/api/v1/search/demo/");

    //some other fields/methods/static blocks e.t.c., for common framework behavior
    //for example static block to wait while mail service will start locally
}
