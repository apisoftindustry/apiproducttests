package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.AppleProductType;
import models.ColaProductType;
import models.PastaProductType;
import models.ProductTypeCommonModel;
import net.serenitybdd.rest.SerenityRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class SanityTestsPositive extends BaseTest {

    private static final Logger logger = LoggerFactory.getLogger(SanityTestsPositive.class);
    private Response response;

    @When("{string} calls endpoint {string}")
    public void userCallsEndpoint(String user, String productType) {
        //Log some common information
        logger.info(user + "calls: " + productType);

        // Perform actual call: GET type
        response = SerenityRest.given().get(BASE_URL + productType);
    }

    @Then("{string} sees the results displayed for productType: orange")
    public void seesTheResultsDisplayedForProductTypeOrange(String user) {
        logger.info("user name: " + user);

        //assert response code
        restAssuredThat(response -> response.statusCode(200));
        //assert body - empty in this case
        restAssuredThat(response -> response.body("", equalTo(Collections.emptyList())));
    }

    @Then("{string} sees the results displayed for productType: apple")
    public void seesTheResultsDisplayedForProductTypeApple(String user) {
        logger.info("user name: " + user);

        //asserts code
        restAssuredThat(response -> response.statusCode(200));

        //get desired object for separate test
        List<AppleProductType> apples = response.jsonPath().getList("", AppleProductType.class);

        assertEquals(9, apples.size());

        assertProductsNotNull(apples);
    }

    @Then("{string} sees the results displayed for productType: pasta")
    public void seesTheResultsDisplayedForProductTypePasta(String user) {
        logger.info("user name: " + user);

        //asserts code
        restAssuredThat(response -> response.statusCode(200));

        //get desired object for separate test
        List<PastaProductType> pastas = response.jsonPath().getList("", PastaProductType.class);

        assertEquals(20, pastas.size());

        assertProductsNotNull(pastas);
    }

    @Then("{string} sees the results displayed for productType: cola")
    public void seesTheResultsDisplayedForProductTypeCola(String user) {
        logger.info("user name: " + user);

        //asserts code
        restAssuredThat(response -> response.statusCode(200));

        //get desired object for separate test
        List<ColaProductType> colas = response.jsonPath().getList("", ColaProductType.class);

        //added failure check here just to see some failed tests in Serenity Report .html file :)
        assertEquals(21, colas.size());

        assertProductsNotNull(colas);
    }

    public static void assertProductsNotNull(List<? extends ProductTypeCommonModel> products) {
        assertNotNull(products);
        products.forEach(SanityTestsPositive::assertProductNotNull);
    }

    public static void assertProductNotNull(ProductTypeCommonModel product) {
        //IMPORTANT:
        //Guys I didn't check each field value for each object from response array,
        //otherwise we need to create hardcoded files (json for example).
        //Put there schema objects and transfer them in real Java object with
        //initialized values and then just compare arrived response with prepared object.
        //This is just one option, there many others.

        //BUT we also have to check values are correct ones in real project!

        assertNotNull(product);
        assertNotNull(product.getProvider());
        assertNotNull(product.getTitle());
        assertNotNull(product.getUrl());
        assertNotNull(product.getBrand());
        assertNotNull(product.getUnit());
        assertNotNull(product.getImage());
    }
}
