package utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateTimeUtils {

    public static final String DEFAULT_TIME_ZONE = "UTC";

    public static LocalDateTime convertToLocal(Date date) {
        if (date == null)
            return null;
        ZoneId zoneId = TimeZone.getTimeZone(DEFAULT_TIME_ZONE).toZoneId();
        return LocalDateTime.from(date.toInstant().atZone(zoneId));
    }

    public static Date convertToDate(LocalDateTime localDateTime) {
        if (localDateTime == null)
            return null;
        ZoneId zoneId = TimeZone.getTimeZone(DEFAULT_TIME_ZONE).toZoneId();
        return Date.from(localDateTime.atZone(zoneId).toInstant());
    }

    public static Date convertToDateFromOffset(OffsetDateTime offsetDateTime) {
        return offsetDateTime == null ? null : Date.from(offsetDateTime.toInstant());
    }

    public static OffsetDateTime convertToOffsetFromDate(Date date) {
        return date == null ? null : date.toInstant().atOffset(ZoneOffset.UTC);
    }

    public static OffsetDateTime convertToOffsetFromLocaleDateTime(LocalDateTime localDateTime) {
        return localDateTime == null ? null : OffsetDateTime.of(Objects.requireNonNull(localDateTime), ZoneOffset.UTC);
    }

    public static boolean sameDateTime(Date date, LocalDateTime localDateTime) {
        Instant dateInstant = date == null ? null : date.toInstant();
        Instant localDateTimeInstant = localDateTime == null ? null : localDateTime.toInstant(ZoneOffset.UTC);
        return Objects.equals(dateInstant, localDateTimeInstant);
    }

}
