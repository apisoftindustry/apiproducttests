FROM maven:3.9.2-eclipse-temurin-17

# Install necessary packages
RUN apt-get update && apt-get install -y \
    wget \
    curl \
    unzip \
    gnupg2 \
    software-properties-common

# Install Google Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN add-apt-repository "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"
RUN apt-get update && apt-get install -y google-chrome-stable

# Install Chrome Driver
ARG LATEST_CHROMEDRIVER="93.0.4577.63"
RUN wget -q "https://chromedriver.storage.googleapis.com/${LATEST_CHROMEDRIVER}/chromedriver_linux64.zip" -O /tmp/chromedriver.zip
RUN unzip /tmp/chromedriver.zip -d /usr/local/bin/
RUN rm /tmp/chromedriver.zip

# Add Jenkins repository key and install Jenkins
RUN wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
RUN echo "deb http://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list

# Import Jenkins repository GPG key manually
RUN gpg --keyserver keyserver.ubuntu.com --recv-keys 5BA31D57EF5975CA
RUN gpg --export --armor 5BA31D57EF5975CA | apt-key add -

# Update package lists
RUN apt-get update

# Install Jenkins
RUN apt-get install -y jenkins

# Expose Jenkins port
EXPOSE 8080
EXPOSE 50000

# Start Jenkins on container startup
CMD ["java", "-jar", "/usr/share/java/jenkins.war"]
