### What was refactored:
if in few words - almost everything Guys - seem like it was an extraction from some existing project you have currently (or had...)
1) update .pom deleted some not used dependencies (those which were aimed for UI testing as here we're doing API for now)
2) removed ALL unused config files across whole project, like: 
- GitHab Action pipline files (for Maven and Gradle) - as we're using GitLab CI\CD for now
- Gradle setup as we have Maven which cover all our needs for now
- unused java classes and packages
- history folder in root - we also don't use it for now
- update .gitignore with 
- update README files
- removed "serenity.properties" file from the root folder - for now also don't used
so tried to do as clean as possible just to concentrate on requirements provide in task 
3) Fixed tests as they were check nothing and designed just to make execution "green"
4) wrote new tests according to requirements from the task (even more then required)
5) created some common models and utils which might be helpful 
6) left 1 test to be failed to see it in result report

### Project tech stack:
- Java 17
- Maven 3.8.6
- Serenity BDD testing
- Cucumber
- log4j
- lombok
- junit
- RestAssured

### How to setup project:
1) Clone repository from [GitLab Artifact](git@gitlab.com:newDevide/sanitytestexample.git)
- using ssh key for your account or via login credentials (access should be granted already if you're reading this)
2) Install and configure locally Java17 or higher and Maven 3.8.6 build tool
3) Open project using any software development environment, like intellij idea or visual studio, e.t.c.
4) Set-up your Java for project SKD setting, the same shoukd be done for Maven 
5) Check than Maven downloaded all dependencies after some time and don't see any errors in logs
6) Go to the root foler of project in terminal and execute command 
```mvn clean verify```
7) This command should create class paths by classloaders and build all dependencies together.
Also it will trigger all tests we have currently in project and generate report in "target" folder

### How to write new tests
1) In order to add new tests we need first to describe them using gherkin format in ".feature" file
(src/test/resources/features/test_sanity.feature) - example below

```Gherkin
Feature: Search by keyword

  Scenario: Searching for a term
    Given Sergey is researching things on the internet
    When he looks up "Cucumber"
    Then he should see information about "Cucumber"
```

2) After we need to create appropriate java methods according to described steps:

```java
    @Given("{actor} is researching things on the internet")
    public void researchingThings(Actor actor) {
        actor.wasAbleTo(NavigateTo.theWikipediaHomePage());
    }

    @When("{actor} looks up {string}")
    public void searchesFor(Actor actor, String term) {
        actor.attemptsTo(
                LookForInformation.about(term)
        );
    }

    @Then("{actor} should see information about {string}")
    public void should_see_information_about(Actor actor, String term) {
        actor.attemptsTo(
                Ensure.that(WikipediaArticle.HEADING).hasText(term)
        );
    }
```

3) To run tests - ```mvn clean verify```

for more official information please refer to - src/test/README(OFFICIAL).md file

### Reporting
Each time you push in "main" GitLab CI\CD starts from this file ".gitlab-ci.yml"
It runs tests and collects reports.
Fresh report available by GitLab Pages functionality, link below:

[api test latest report](https://apisoftindustry.gitlab.io/apiproducttests/index.html)

all piplines for project:
[piplines](https://gitlab.com/apisoftindustry/apiproducttests/-/pipelines)

p.s.: if you want to see report by pipline execution - open specific piplne and download artifacts - there is a report for specific run
we also might need to update reporting mechanism in future to be able see reports already deployed and grouped by pipline execution - so don't need to download them each time or see only the latest one
(Allure\Junit reports, Jenkins\GitHub Actions\custom tool to work with .xml\.html files as CI\CD)

### Some thoughts about CI\CD:
If in future we will need some UI scenarios as well we can prepare our own docker image with all needed setup stuff (browser\driver\buid tool e.t.c.
Something like Dockerfile file in root of directory.
Or use an existing one from dockerhub storage.
